package com.sdkjordan.alkaff.justworkshop.fragment;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;

import com.sdkjordan.alkaff.justworkshop.R;


//Several Activity lifecycle methods are instrumented to emit LogCat output
//so you can follow this class' lifecycle
public class ProgrammaticFeaturesViewerActivity extends Activity implements TitlesFragment.ListListener {

	private static final String TAG = "fragments";
	// get class name automatically to be used for logging
	public static  String CLASS ;   { CLASS = this.getClass().getSimpleName() ; }


	public static String[] mTitleArray;
	public static String[] mFeatureArray;

	// Get a reference to the FeaturesFragment
	private final FeaturesFragment mFeatureFragment = new FeaturesFragment();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		// Get the string arrays with the titles and features
		mTitleArray = getResources().getStringArray(R.array.Titles);
		mFeatureArray = getResources().getStringArray(R.array.Features);
		
		setContentView(R.layout.main_programmatic);

		// Get a reference to the FragmentManager
		FragmentManager fragmentManager = getFragmentManager();
		
		// Begin a new FragmentTransaction
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();
		
		// Add the TitleFragment
		fragmentTransaction.add(R.id.title_frame, new TitlesFragment());

		// Add the FeatureFragment
		fragmentTransaction.add(R.id.Feature_frame, mFeatureFragment);
		
		// Commit the FragmentTransaction
		fragmentTransaction.commit();
	}

	// Called when the user selects an item in the TitlesFragment
	@Override
	public void onListSelection(int index) {
		if (mFeatureFragment.getShownIndex() != index) {
	
			// Tell the FeatureFragment to show the Feature string at position index
			mFeatureFragment.showFeatureAtIndex(index);
		}
	}
	@Override
	public String[] getTitleList() {
		return mTitleArray;
	}

	@Override
	public String[] getDataList() {
		return mFeatureArray;
	}

	@Override
	protected void onDestroy() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDestroy();

	}

	@Override
	protected void onPause() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onPause();

	}

	@Override
	protected void onRestart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onRestart();

	}

	@Override
	protected void onResume() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onResume();
	}

	@Override
	protected void onStart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onStart();
	}

	@Override
	protected void onStop() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		super.onStop();
	}

}