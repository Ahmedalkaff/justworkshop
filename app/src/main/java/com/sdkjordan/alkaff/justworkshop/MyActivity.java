package com.sdkjordan.alkaff.justworkshop;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sdkjordan.alkaff.justworkshop.fragment.DynamicFeatureViewerActivity;
import com.sdkjordan.alkaff.justworkshop.fragment.ProgrammaticFeaturesViewerActivity;
import com.sdkjordan.alkaff.justworkshop.fragment.StaticFeatureViewerActivity;
import com.sdkjordan.alkaff.justworkshop.fragment.TitlesListActivity;

public class MyActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "fragments";

    //action could be any string, but it is recommended to prefixed it with package name to avoid any conflicts with others actions
    private static final String MY_ACTION = "com.sdkjordan.alkaff.justworkshop.my_action";

    // get class name automatically to be used for logging
    public static  String CLASS ;   { CLASS = this.getClass().getSimpleName() ; }

    private static final int REQUEST_CODE1 = 10;
    public static final String EXTRA_KEY = "data" ;

    private EditText mEditTextData ;

    private TextView mTextViewResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout);
        // Automatic log
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

        // To load the previous saved state from the last start of the activity
        if(savedInstanceState != null)
        {
            String date = savedInstanceState.getString("Key");
        }

        // do any initialization inside this method
        init();

    }

    private void init() {
        mEditTextData = findViewById(R.id.editTextData);

        mTextViewResult = findViewById(R.id.textViewResult);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("Key","value");
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
        if(requestCode == REQUEST_CODE1)
        {
            switch (resultCode)
            {
                case RESULT_OK:
                    if(data != null) {
                        String result = data.getStringExtra(EXTRA_KEY);
                        if(result!= null)
                            mTextViewResult.setText(result);
                        else
                            mTextViewResult.setText(getResources().getText(R.string.no_data));

                    }
                    break;
                case RESULT_FIRST_USER:

                    break;
                case RESULT_CANCELED:
                    break;

            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

    }



    @Override
    public void onClick(View view) {

        Intent intent = null ;
        switch (view.getId())
        {
            case R.id.buttonActivity:
                // this is an implicit intent , which could be used inside or out side the application
                // you have to add intent filter to the target activity to receive the action like this in manifest
                //          <intent-filter>
                //                <action android:name="com.sdkjordan.alkaff.justworkshop.my_action"></action>
                //                <category android:name="android.intent.category.DEFAULT"></category>
                //            </intent-filter>
                intent = new Intent(MY_ACTION);
                //TODO: it is recommenced to check the data before sending it.
                intent.putExtra(EXTRA_KEY,mEditTextData.getText().toString());
                startActivityForResult(intent,REQUEST_CODE1);
                break;
            case R.id.buttonUsingActivities:
                intent = new Intent(MyActivity.this,TitlesListActivity.class);
                startActivity(intent);
                break;
            case R.id.buttonStaticFragment:
                intent = new Intent(MyActivity.this,StaticFeatureViewerActivity.class);
                startActivity(intent);
                break;
            case R.id.buttonDynamicFragment:
                intent = new Intent(MyActivity.this,DynamicFeatureViewerActivity.class);
                startActivity(intent);
                break;
            case R.id.buttonProgrammaticallyFragment:
                intent = new Intent(MyActivity.this,ProgrammaticFeaturesViewerActivity.class);
                startActivity(intent);
                break;
        }
    }


}
