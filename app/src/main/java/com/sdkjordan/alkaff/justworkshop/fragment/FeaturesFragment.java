package com.sdkjordan.alkaff.justworkshop.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sdkjordan.alkaff.justworkshop.R;

//Several Activity and Fragment lifecycle methods are instrumented to emit LogCat output
//so you can follow the class' lifecycle
public class FeaturesFragment extends Fragment {

	private static final String TAG = "fragments";


	// get class name automatically to be used for logging
	public static  String CLASS ;   { CLASS = this.getClass().getSimpleName() ; }



	private TextView mFeatureView = null;
	private int mCurrIdx = -1;
	private int mFeatureArrayLen;

	String[] mFeatureList ;

	public int getShownIndex() {
		return mCurrIdx;
	}

	// Show the Feature string at position newIndex
	public void showFeatureAtIndex(int newIndex) {
		if (newIndex < 0 || newIndex >= mFeatureArrayLen)
			return;
		mCurrIdx = newIndex;
		mFeatureView.setText(mFeatureList[mCurrIdx]);
	}

	@Override
	public void onAttach(Activity activity) {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onAttach(activity);



	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
	}

	// Called to create the content view for this Fragment
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		// Inflate the layout defined in features_fragmentent.xml
		// The last parameter is false because the returned view does not need to be attached to the container ViewGroup
		return inflater.inflate(R.layout.features_fragment, container, false);
	}

	// Set up some information about the mFeatureView TextView
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		if(getActivity() instanceof TitlesFragment.ListListener)
		{
			mFeatureList = 	((TitlesFragment.ListListener) getActivity()).getDataList();
			mFeatureArrayLen = mFeatureList.length ;
			mFeatureView = (TextView) getActivity().findViewById(R.id.featureView);

		}else {
			throw new ClassCastException(getActivity().toString()
					+ " must implement OnArticleSelectedListener");
		}

	}

	@Override
	public void onStart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onStart();

	}

	@Override
	public void onResume() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onResume();
	}

	@Override
	public void onPause() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onPause();
	}

	@Override
	public void onStop() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onStop();

	}

	@Override
	public void onDetach() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDetach();
	}

	@Override
	public void onDestroy() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDestroyView();
	}

}
