package com.sdkjordan.alkaff.justworkshop.fragment;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.sdkjordan.alkaff.justworkshop.R;

public class TitlesListActivity extends ListActivity {

	public static String[] mTitleArray;
	public static String[] mFeaturesArray;

	public static final String INDEX = "index";

	private static final String TAG = "fragments";

	// get class name automatically to be used for logging
	public static  String CLASS ;   { CLASS = this.getClass().getSimpleName() ; }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		// Get the string arrays with the titles and features
		mTitleArray = getResources().getStringArray(R.array.Titles);
		mFeaturesArray = getResources().getStringArray(R.array.Features);

		// Set the list adapter for the ListView 
		setListAdapter(new ArrayAdapter<String>(TitlesListActivity.this,
				android.R.layout.simple_list_item_activated_1,android.R.id.text1, TitlesListActivity.mTitleArray));

	}

	@Override
	public void onListItemClick(ListView l, View v, int pos, long id) {
		
		// Create implicity Intent to start the FeatureListActivity class
		Intent showItemIntent = new Intent(TitlesListActivity.this,
				FeaturesListActivity.class);
		
		// Add an Extra representing the currently selected position
		// The name of the Extra is stored in INDEX
		showItemIntent.putExtra(INDEX, mFeaturesArray[pos]);
		
		// Start the FeatureListActivity using Activity.startActivity()
		startActivity(showItemIntent);
	}


	@Override
	protected void onDestroy() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDestroy();

	}

	@Override
	protected void onPause() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onPause();

	}

	@Override
	protected void onRestart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onRestart();

	}

	@Override
	protected void onResume() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onResume();
	}

	@Override
	protected void onStart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onStart();
	}

	@Override
	protected void onStop() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		super.onStop();
	}
}
