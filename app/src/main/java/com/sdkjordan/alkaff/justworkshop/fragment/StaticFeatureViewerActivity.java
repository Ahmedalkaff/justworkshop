package com.sdkjordan.alkaff.justworkshop.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.sdkjordan.alkaff.justworkshop.R;

//Several Activity lifecycle methods are instrumented to emit LogCat output
//so you can follow this class' lifecycle
public class StaticFeatureViewerActivity extends AppCompatActivity implements
        TitlesFragment.ListListener {

	private static final String TAG = "fragments";
	// get class name automatically to be used for logging
	public static  String CLASS ;   { CLASS = this.getClass().getSimpleName() ; }

	public static String[] mTitleArray;
	public static String[] mFeatureArray;
	private FeaturesFragment mDetailsFragment;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_static);

		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		// Get the string arrays with the titles and features
		mTitleArray = getResources().getStringArray(R.array.Titles);
		mFeatureArray = getResources().getStringArray(R.array.Features);
		

		// Get a reference to the FeaturesFragment
		mDetailsFragment = (FeaturesFragment) getFragmentManager()
				.findFragmentById(R.id.details);
	}

	// Called when the user selects an item in the TitlesFragment
	@Override
	public void onListSelection(int index) {
		if (mDetailsFragment.getShownIndex() != index) {

			// Tell the FeatureFragment to show the Feature string at position index
			mDetailsFragment.showFeatureAtIndex(index);
		}
	}

	@Override
	public String[] getTitleList() {
		return mTitleArray;
	}

	@Override
	public String[] getDataList() {
		return mFeatureArray;
	}

	@Override
	protected void onDestroy() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDestroy();

		}

	@Override
	protected void onPause() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onPause();

	}

	@Override
	protected void onRestart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onRestart();

	}

	@Override
	protected void onResume() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onResume();
	}

	@Override
	protected void onStart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onStart();
	}

	@Override
	protected void onStop() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		super.onStop();
	}

}