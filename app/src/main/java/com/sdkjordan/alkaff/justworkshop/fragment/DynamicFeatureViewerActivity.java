package com.sdkjordan.alkaff.justworkshop.fragment;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.sdkjordan.alkaff.justworkshop.R;

//Several Activity lifecycle methods are instrumented to emit LogCat output
//so you can follow this class' lifecycle
public class DynamicFeatureViewerActivity extends AppCompatActivity implements
		TitlesFragment.ListListener {


	private static final String TAG = "fragments";
	// get class name automatically to be used for logging
	public static  String CLASS ;   { CLASS = this.getClass().getSimpleName() ; }

	public static String[] mTitleArray;
	public static String[] mFeatureArray;


	private final FeaturesFragment mFeatureFragment = new FeaturesFragment();
	private FragmentManager mFragmentManager;
	private FrameLayout mTitleFrameLayout, mFeaturesFrameLayout;

	private static final int MATCH_PARENT = LinearLayout.LayoutParams.MATCH_PARENT;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_dynamic);

		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		// Get the string arrays with the titles and features
		mTitleArray = getResources().getStringArray(R.array.Titles);
		mFeatureArray = getResources().getStringArray(R.array.Features);


		// Get references to the TitleFragment and to the FeaturesFragment
		mTitleFrameLayout = (FrameLayout) findViewById(R.id.title_fragment_container);
		mFeaturesFrameLayout = (FrameLayout) findViewById(R.id.Feature_fragment_container);


		// Get a reference to the FragmentManager
		mFragmentManager = getFragmentManager();

		// Start a new FragmentTransaction
		FragmentTransaction fragmentTransaction = mFragmentManager
				.beginTransaction();

		// Add the TitleFragment to the layout
		fragmentTransaction.add(R.id.title_fragment_container,
				new TitlesFragment());
		
		// Commit the FragmentTransaction
		fragmentTransaction.commit();

		// Add a OnBackStackChangedListener to reset the layout when the back stack changes
		mFragmentManager
				.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
					public void onBackStackChanged() {
						setLayout();
					}
				});
	}

	private void setLayout() {
		
		// Determine whether the FeatureFragment has been added
		if (!mFeatureFragment.isAdded()) {
			
			// Make the TitleFragment occupy the entire layout 
			mTitleFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(
					MATCH_PARENT, MATCH_PARENT));
			mFeaturesFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(0,
					MATCH_PARENT));
		} else {

			// Make the TitleLayout take 1/3 of the layout's width
			mTitleFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(0,
					MATCH_PARENT, 1f));
			
			// Make the FeatureLayout take 2/3's of the layout's width
			mFeaturesFrameLayout.setLayoutParams(new LinearLayout.LayoutParams(0,
					MATCH_PARENT, 2f));
		}
	}

	// Called when the user selects an item in the TitlesFragment
	@Override
	public void onListSelection(int index) {

		// If the FeatureFragment has not been added, add it now
		if (!mFeatureFragment.isAdded()) {
		
			// Start a new FragmentTransaction
			FragmentTransaction fragmentTransaction = mFragmentManager
					.beginTransaction();

			// Add the FeatureFragment to the layout
			fragmentTransaction.add(R.id.Feature_fragment_container,
					mFeatureFragment);

			// Add this FragmentTransaction to the backstack
			fragmentTransaction.addToBackStack(null);
			
			// Commit the FragmentTransaction
			fragmentTransaction.commit();
			
			// Force Android to execute the committed FragmentTransaction
			mFragmentManager.executePendingTransactions();
		}
		
		if (mFeatureFragment.getShownIndex() != index) {
			// Tell the FeatureFragment to show the Feature string at position index
			mFeatureFragment.showFeatureAtIndex(index);
		}
	}

	@Override
	public String[] getTitleList() {
		return mTitleArray;
	}

	@Override
	public String[] getDataList() {
		return mFeatureArray;
	}


	@Override
	protected void onDestroy() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onPause();
	}

	@Override
	protected void onRestart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onRestart();
	}

	@Override
	protected void onResume() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onResume();
	}

	@Override
	protected void onStart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onStart();
	}

	@Override
	protected void onStop() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onStop();
	}

}