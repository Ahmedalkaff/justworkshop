package com.sdkjordan.alkaff.justworkshop.fragment;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.sdkjordan.alkaff.justworkshop.R;

public class FeaturesListActivity extends ListActivity {

	private static final String TAG = "fragments";

	// get class name automatically to be used for logging
	public static  String CLASS ;   { CLASS = this.getClass().getSimpleName() ; }

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );


		// Get the Intent that started this Activity
		Intent intent = getIntent();
		
		// Retrieve the Extra stored under the name TitlesListActivity.INDEX
		String feature = intent.getStringExtra(TitlesListActivity.INDEX);

		
		if (null != feature) {
			
			// Set the list adapter for the ListView 
			setListAdapter(new ArrayAdapter<String>(FeaturesListActivity.this,
					android.R.layout.simple_list_item_activated_1,android.R.id.text1, new String[] { feature }));
		}
	}


	@Override
	protected void onDestroy() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDestroy();

	}

	@Override
	protected void onPause() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onPause();

	}

	@Override
	protected void onRestart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onRestart();

	}

	@Override
	protected void onResume() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onResume();
	}

	@Override
	protected void onStart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onStart();
	}

	@Override
	protected void onStop() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		super.onStop();
	}
}