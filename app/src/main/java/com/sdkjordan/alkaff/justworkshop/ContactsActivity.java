package com.sdkjordan.alkaff.justworkshop;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ContactsActivity extends AppCompatActivity {

    @SuppressLint("InlinedApi")
    private final static String NAME = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :ContactsContract.Contacts.DISPLAY_NAME;

    @SuppressLint("InlinedApi")
    private final static String[] FROM_COLUMNS = {ContactsContract.Contacts._ID ,NAME } ;
    private static final int READ_CONTACT_REQ =  10;


    ContentResolver contentResolver ;

    SimpleCursorAdapter mSimpleCursorAdapter ;
    FloatingActionButton MActionButton ;
    ListView mListViewContacts ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        MActionButton = findViewById(R.id.floatingActionButton);
        mListViewContacts = findViewById(R.id.Listconstats);

        contentResolver = getContentResolver() ;

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M )
        {
            if(ActivityCompat.checkSelfPermission(getApplicationContext(),Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
            {
                if(ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_CONTACTS))
                {
                    Toast.makeText(this,"Need permission to read contacts",Toast.LENGTH_SHORT).show();
                    return;
                }

                ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS},READ_CONTACT_REQ);
                return;
            }
        }
        LoadContacts();


    }


    private List<String> contact = new ArrayList<>();
    private void LoadContacts()
    {
        Cursor c =  contentResolver.query(ContactsContract.Contacts.CONTENT_URI,FROM_COLUMNS,null,null,null);
        Log.e("justworkshop",String.valueOf(c.getColumnCount()));
        c.moveToFirst() ;
        contact.clear();


        while(c.moveToNext())
        {
            contact.add(c.getString(1) == null ? "No name" : c.getString(1));
            Log.e("justworkshop",c.getString(1) == null ? "No name" : c.getString(1));
        }


//        CursorLoader cursorLoader = new CursorLoader(this,ContactsContract.Contacts.CONTENT_URI,FROM_COLUMNS,null,null,null);


//        mSimpleCursorAdapter = new SimpleCursorAdapter(this,R.layout.list_item,cursorLoader.loadInBackground(),new String[]{NAME},new int[R.id.textViewName],0);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getApplication(),R.layout.list_item,R.id.textViewName,contact);
        mListViewContacts.setAdapter(arrayAdapter);

//        if(mSimpleCursorAdapter != null)
//            mListViewContacts.setAdapter(mSimpleCursorAdapter);
    }

    abstract  class  A {
        int x ;
        abstract void method();
    }

    private  void me(A a)
    {

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == READ_CONTACT_REQ && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            LoadContacts() ;
        }else
            Toast.makeText(this,"Unable  to read contacts",Toast.LENGTH_SHORT).show();

    }
}
