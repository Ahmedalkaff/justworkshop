package com.sdkjordan.alkaff.justworkshop.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;


//Several Activity and Fragment lifecycle methods are instrumented to emit LogCat output
//so you can follow the class' lifecycle
public class TitlesFragment extends ListFragment {


	private static final String TAG = "fragments";
	// get class name automatically to be used for logging
	public static  String CLASS ;   { CLASS = this.getClass().getSimpleName() ; }


	private ListListener mListener = null;


	// Callback interface that allows this Fragment to notify the host Activity when
	// user clicks on a List Item  
	public interface ListListener {
		 void onListSelection(int index);
		String[] getTitleList();
		String[] getDataList();
	}



	// Called when the user selects an item from the List
	@Override
	public void onListItemClick(ListView l, View v, int pos, long id) {

		// Indicates the selected item has been checked
		getListView().setItemChecked(pos, true);
		
		// Inform the Hosted activity that the item in position pos has been selected
		mListener.onListSelection(pos);
	}
	
	@Override
	public void onAttach(Activity activity) {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onAttach(activity);
		try {

			// Set the ListSelectionListener for communicating with the FeatureViewerActivity
			mListener = (ListListener) activity;
		
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnArticleSelectedListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedState) {
		super.onActivityCreated(savedState);
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		// Set the list choice mode to allow only one selection at a time
		getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		// Set the list adapter for the ListView
		setListAdapter(new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_activated_1,android.R.id.text1, mListener.getTitleList()));
	}
	
	@Override
	public void onStart() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		super.onStart();
	}

	@Override
	public void onResume() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		super.onResume();
	}

	@Override
	public void onPause() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onPause();
	}

	@Override
	public void onStop() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		super.onStop();
	}

	@Override
	public void onDetach() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

		super.onDetach();
	}

	@Override
	public void onDestroy() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		// Automatic log
		Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
		super.onDestroyView();
	}
}