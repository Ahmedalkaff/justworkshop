package com.sdkjordan.alkaff.justworkshop;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "fragments";
    // get class name automatically to be used for logging
    public static  String CLASS ;   { CLASS = this.getClass().getSimpleName() ; }

    private String data  ;
    private EditText mEditTextInput ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mEditTextInput = findViewById(R.id.editTextInput);
        Intent intent = getIntent();
        if(intent != null)
        {
            data = intent.getStringExtra(MyActivity.EXTRA_KEY);
            if(data != null)
                mEditTextInput.setText(data);
            else
                mEditTextInput.setHint(getResources().getString(R.string.no_data));
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
    }


    @Override
    protected void onStart() {
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
        super.onStart();

    }

    @Override
    protected void onStop() {
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

        super.onDestroy();

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

        super.onBackPressed();
   }

    @Override
    protected void onPause() {
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

        super.onPause();
    }

    @Override
    protected void onResume() {
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );

        super.onResume();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG,CLASS+ "-->"+ new Object(){}.getClass().getEnclosingMethod().getName() );
        super.onRestart();

    }

    @Override
    public void onClick(View view) {

        data = mEditTextInput.getText().toString();
        if(data != null )
        {
            mEditTextInput.setText("");

            // no need to add action or components to the Intent
            // since the activity is started for result and it will return to the called activity once this activity is finished
            Intent intent = new Intent();
            intent.putExtra(MyActivity.EXTRA_KEY,data);
            setResult(RESULT_OK,intent);
        }else
            setResult(RESULT_OK);
        // once you call finish it will send the result back to the called activity
        finish();
    }
}
